import { FC } from "react";
import InfiniteScroll from "react-infinite-scroll-component";

import "src/components/home/home.scss";
import Card from "src/components/shared/Card";

import { IResponse } from "src/models/response.model";
import Loader from "src/components/shared/Loader";

interface IHomeProps {
  books: IResponse[];
  hasMore: boolean;
  fetchMoreData: () => void;
}

const Home: FC<IHomeProps> = ({ books, hasMore, fetchMoreData }) => {
  return (
    <div className="home" data-testid="home">
      <div className="home__container">
        <div>
          <InfiniteScroll
            dataLength={books.length ?? 0}
            next={fetchMoreData}
            hasMore={hasMore}
            loader={<Loader />}
            endMessage={
              <p
                style={{
                  textAlign: "center",
                  alignSelf: "flex-end",
                  margin: "4rem 0",
                  color: "red",
                  fontSize: "2rem",
                }}
              >
                <b>THE END</b>
              </p>
            }
            className="home__container--content"
          >
            {books?.map((book, i) => (
              <Card book={book} key={i} />
            ))}
          </InfiniteScroll>
        </div>
      </div>
    </div>
  );
};

export default Home;
