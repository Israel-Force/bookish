import Container from "typedi";

import SearchService from "src/api/services/search";
import { Observable } from "rxjs";
import { IResponse } from "src/models/response.model";

const searchServiceInstance = Container.get(SearchService);

const selectType = (key: string, value: any): Observable<IResponse[]> => {
  switch (key) {
    case "book_name":
      return searchServiceInstance.getBooksByQuery("name", value);
    case "publisher":
      return searchServiceInstance.getBooksByQuery(key, value);
    case "isbn":
      return searchServiceInstance.getBooksByQuery(key, value);
    case "author":
      return searchServiceInstance.getBooksByQuery(key, value);
    case "character_name":
      return searchServiceInstance.getBooksByCharacter("name", value);
    case "character_culture":
      return searchServiceInstance.getBooksByCharacter("culture", value);
    default:
      return searchServiceInstance.getBooksByQuery(key, value);
  }
};

export default selectType;
