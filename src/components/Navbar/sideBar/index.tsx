import React, { FC, useEffect } from "react";

import "./sidebar.scss";
import SearchComponent from "src/components/shared/SearchComponent";
import selectType from "../helper/selectType";
import { IResponse } from "src/models/response.model";
import Loader from "src/components/shared/Loader";

interface INavbarProps {
  updateData: (newData: IResponse[]) => void;
}

const SideBar: FC<INavbarProps> = ({ updateData }) => {
  const [open, setOpen] = React.useState(false);
  const [loading, setLoading] = React.useState(false);

  const handleClick = () => {
    setOpen(!open);
  };

  const searchForBooks = () => {
    let type = (
      document.querySelector('input[name="type"]:checked') as HTMLInputElement
    ).value;
    let searchInput = (document.getElementById("input") as HTMLInputElement)
      .value;
    setLoading(true);
    return selectType(type, searchInput).subscribe((res: any) => {
      updateData(res);
      setLoading(false);
      setOpen(false);
    });
  };

  useEffect(() => {
    return () => {
      searchForBooks().unsubscribe();
    };
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <div className="sidebar_wrapper">
      <div className="menu-btn" onClick={handleClick}>
        <span
          className={open ? "menu-btn_burger open" : "menu-btn_burger"}
        ></span>
      </div>
      <div>
        {open && (
          <>
            <div className="aside">
              <SearchComponent searchForBooks={searchForBooks} />
            </div>
            {loading && <Loader />}
          </>
        )}
      </div>
    </div>
  );
};

export default SideBar;
