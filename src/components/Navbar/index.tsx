import { FC } from "react";

import Toolbar from "./Toolbar";
import SideNav from "./sideBar";
import { IResponse } from "src/models/response.model";

interface INavbarProps {
  updateData: (newData: IResponse[]) => void;
}

const Navbar: FC<INavbarProps> = ({ updateData }) => {
  return (
    <div data-testid="navbar">
      <SideNav updateData={updateData} />
      <Toolbar updateData={updateData} />
    </div>
  );
};

export default Navbar;
