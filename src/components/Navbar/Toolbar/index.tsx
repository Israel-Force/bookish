import { FC, useEffect, useState } from "react";
import { debounceTime, distinctUntilChanged, fromEvent, switchMap } from "rxjs";

import "./toolbar.scss";
import { Links } from "src/components/shared/styledComponents";
import logo from "src/assets/imgs/logo.png";
import selectType from "../helper/selectType";
import { IResponse } from "src/models/response.model";
import SearchComponent from "src/components/shared/SearchComponent";

interface INavbarProps {
  updateData: (newData: IResponse[]) => void;
}

const Toolbar: FC<INavbarProps> = ({ updateData }) => {
  const [top, setTop] = useState(true);

  useEffect(() => {
    window.onscroll = () => {
      if (window.pageYOffset === 0) setTop(true);
      else setTop(false);
    };

    return () => {
      window.onscroll = null;
    };
  });

  const scroll = (height: number) => {
    window.scrollTo({ top: height, behavior: "smooth" });
  };

  const searchForBooks = () => {
    let type = (
      document.querySelector('input[name="type"]:checked') as HTMLInputElement
    ).value;
    let searchInput = (document.getElementById("input") as HTMLInputElement)
      .value;
    return selectType(type, searchInput);
  };

  useEffect(() => {
    const sub$ = fromEvent(
      document.getElementById("input") as HTMLInputElement,
      "keyup"
    )
      .pipe(
        debounceTime(350),
        distinctUntilChanged(),
        switchMap((val) => searchForBooks())
      )
      .subscribe((res: any) => {
        updateData(res);
        scroll(500);
      });
    return () => sub$.unsubscribe();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const useBoxShadow = {
    boxShadow: top ? "none" : "0px 3px 6px rgba(0, 0, 0, 0.05)",
  };

  return (
    <div className="toolbar_wrapper" style={useBoxShadow}>
      <Links>
        <div className="logo">
          <img src={logo} alt="inventor logo" />
        </div>
        <div className="center">
          <li onClick={() => scroll(0)}>Home</li>
          <li onClick={() => scroll(500)}>Browse</li>
        </div>
        <div className="right">
          <SearchComponent searchForBooks={searchForBooks} />
        </div>
      </Links>
    </div>
  );
};

export default Toolbar;
