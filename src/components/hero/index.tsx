import { FC } from "react";

import "./hero.scss";
import books from "src/assets/imgs/books.png";

const Hero: FC = () => {
  const scrollDown = () => {
    window.scrollTo({ top: 350, behavior: "smooth" });
  };

  return (
    <div className="hero" data-testid="hero">
      <div className="hero__left">
        <div className="title">
          <p>THE BOOKISH CLUB</p>
        </div>
        <div className="msg">
          <p>The Book Store for the</p>
          <p>BookISH</p>
        </div>
        <div className="btn" onClick={scrollDown}>
          <div>Browse</div>
        </div>
      </div>
      <div className="hero__right">
        <img src={books} alt="" />
      </div>
    </div>
  );
};

export default Hero;
