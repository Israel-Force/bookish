import React from "react";
import styled from "styled-components";

import colors from "src/models/colors";

function Loader() {
  return (
    <LoaderWrapper>
      <div className="lds-hourglass"></div>
    </LoaderWrapper>
  );
}

const LoaderWrapper = styled.section`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 4rem 0;
  .lds-hourglass {
    display: inline-block;
    position: relative;
    width: 8rem;
    height: 8rem;
  }
  .lds-hourglass:after {
    content: " ";
    display: block;
    border-radius: 50%;
    width: 0;
    height: 0;
    margin: 0.8rem;
    box-sizing: border-box;
    border: 3.2rem solid ${colors.colorBlack};
    border-color: ${colors.colorBlack} transparent ${colors.colorBlack}
      transparent;
    animation: lds-hourglass 1.2s infinite;
  }
  @keyframes lds-hourglass {
    0% {
      transform: rotate(0);
      animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);
    }
    50% {
      transform: rotate(900deg);
      animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
    }
    100% {
      transform: rotate(1800deg);
    }
  }
`;

export default Loader;
