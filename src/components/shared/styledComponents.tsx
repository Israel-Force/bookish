import styled from "styled-components";

import colors from "src/models/colors";

export const Links = styled.ul<{ sidenav?: boolean }>`
  display: flex;
  flex-direction: ${(props) => (props.sidenav ? "column" : "row")};
  align-items: ${(props) => (props.sidenav ? "flex-start" : "center")};
  a {
    text-decoration: none;
    &:hover {
      color: ${colors.colorOrange};
    }
  }
  li {
    list-style: none;
    margin-right: 3rem;
    margin-bottom: ${(props) => (props.sidenav ? "2rem" : "0")};
    cursor: pointer;
    &:hover {
      color: ${colors.colorOrange};
    }
  }
`;
