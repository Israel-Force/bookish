import { FC } from "react";
import styled from "styled-components";

import cover from "src/assets/imgs/cover.jpg";
import colors from "src/models/colors";
import { IResponse } from "src/models/response.model";

interface ICardProps {
  book: IResponse;
}

const Card: FC<ICardProps> = ({ book }) => {
  const strimDate = (val: string) => {
    return val.split("T").shift();
  };

  return (
    <Wrapper>
      <section className="card__container">
        <section className="card__container--thumbnail">
          <img src={cover} alt="bookish" />
        </section>
        <section className="card__container--body">
          <div className="card__container--body-cover">
            <p>{book.mediaType}</p>
          </div>
          <div className="card__container--body-title">
            <h2>{book.name}</h2>
          </div>
          <div className="card__container--body-author">
            {book.authors.map((author, i) => (
              <p key={i}>{author}</p>
            ))}
          </div>
          <div className="">
            <span>{book.publisher}</span>
          </div>
        </section>
        <section className="card__container--footer">
          <div className="card__container--footer-left">
            <p>Isbn</p>
            <p>{book.isbn}</p>
          </div>
          <div className="card__container--footer-right">
            <p>released</p>
            <p>{strimDate(book.released)}</p>
          </div>
        </section>
      </section>
    </Wrapper>
  );
};

const Wrapper = styled.section`
  .card {
    &__container {
      transition: all 0.2s ease-in-out;
      padding: 2rem;
      display: flex;
      flex-direction: column;
      border: 0.1rem solid rgba(${colors.colorBlackHex}, 0.1);
      position: relative;
      height: 100%;
      &--thumbnail {
        display: flex;
        align-items: center;
        justify-content: center;
        width: 100%;
      }
      &--body {
        transition: all 0.2s ease-in-out;
        position: relative;
        z-index: 1;
        padding: 1.5rem 2rem 1rem;
        &-cover {
          color: ${colors.colorOrange1};
          font-size: 1.3rem;
          margin-bottom: 0.5rem;
        }
        &-title {
          color: rgba(${colors.colorBlackHex}, 0.7);
          margin-bottom: 1rem;
        }
        &-author {
          color: rgba(${colors.colorBlackHex}, 0.8);
          font-size: 1.2rem;
        }
      }
      &--footer {
        transition: all 0.2s ease-in-out;
        position: absolute;
        padding: 2rem 4rem;
        right: 0;
        left: 0;
        bottom: 0;
        opacity: 0;
        display: flex;
        justify-content: space-between;
        align-items: center;
        & > div {
          p:first-of-type {
            font-size: 0.9rem;
            font-weight: bolder;
            margin-bottom: 0.3rem;
          }
        }
        &-right {
          text-align: right;
        }
      }
    }
  }
  .card__container:hover {
    box-shadow: 0 0 40px 0 rgba(22, 22, 25, 0.1);
    border: 0.1rem solid rgba(${colors.colorBlackHex}, 0.5);
    .card__container--body {
      -webkit-transform: translateY(-44px);
      transform: translateY(-44px);
      background-color: #fff;
    }
    .card__container--footer {
      opacity: 1;
    }
  }
`;

export default Card;
