import React, { FC } from "react";
import styled from "styled-components";
import colors from "src/models/colors";

interface INoDataPropos {
  action: () => void;
}

const NoData: FC<INoDataPropos> = ({ action }) => {
  return (
    <NoDataWrapper>
      <p> No data Found</p>
      <div className="btn" onClick={action}>
        <div>Browse</div>
      </div>
    </NoDataWrapper>
  );
};

const NoDataWrapper = styled.section`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 4rem 0;
  p {
    font-size: 3.5rem;
    margin-bottom: 1rem;
  }
  .btn {
    padding: 1.3rem 3rem;
    color: ${colors.colorWhite};
    border-radius: 0.3rem;
    font-size: 1.4rem;
    font-weight: bold;
    background-color: ${colors.colorBlack};
    cursor: pointer;
    text-align: center;
    max-width: 15rem;
    width: 100%;
  }
`;

export default NoData;
