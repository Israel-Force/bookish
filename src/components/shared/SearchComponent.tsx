import { ChangeEvent, FC, useState } from "react";
import styled from "styled-components";

import arrow_drop_up_black from "src/assets/icons/arrow_drop_up_black.svg";
import arrow_drop_down_black from "src/assets/icons/arrow_drop_down_black.svg";
import colors from "src/models/colors";
import search from "src/assets/icons/search.svg";

interface ISearchComponent {
  searchForBooks: () => void;
}

const SearchComponent: FC<ISearchComponent> = ({ searchForBooks }) => {
  const [searchInput, setsearchInput] = useState("");
  const [searchType, setsearchType] = useState("");
  const [hideDropDown, setHideDropDown] = useState(true);

  const handleChange = (e: ChangeEvent<HTMLInputElement>, type?: string) => {
    if (type === "radio") {
      setsearchType(e.target.value);
      setHideDropDown(true);
      (document.getElementById("input") as HTMLInputElement).focus();
    } else {
      if (searchType !== "") {
        setsearchInput(e.target.value);
      }
    }
  };
  const toggleDropDownOnFirstClickOfSearchInput = () => {
    if (searchType === "") setHideDropDown(false);
  };
  const toggleDropDown = () => {
    setHideDropDown(!hideDropDown);
  };

  return (
    <SearchWrapper>
      <div className="form_group">
        <div className="input_group">
          <span onClick={searchForBooks}>
            <img src={search} alt="search icon" />
          </span>
          <input
            type="text"
            placeholder="Select Type and Search for books..."
            id="input"
            value={searchInput}
            onChange={($event) => handleChange($event)}
            onClick={toggleDropDownOnFirstClickOfSearchInput}
          />
          <span onClick={toggleDropDown}>
            <img
              src={hideDropDown ? arrow_drop_down_black : arrow_drop_up_black}
              alt="search icon right"
            />
          </span>
          <div className={hideDropDown ? "hide" : "dropdown"}>
            <p>Search By</p>
            <div className="form_radio">
              <input
                type="radio"
                name="type"
                id="book_name"
                value="book_name"
                onChange={(e) => handleChange(e, "radio")}
              />
              <label htmlFor="book_name">Book Name</label>
            </div>
            <div className="form_radio">
              <input
                type="radio"
                name="type"
                id="publisher"
                value="publisher"
                onChange={(e) => handleChange(e, "radio")}
              />
              <label htmlFor="publisher">Publisher</label>
            </div>
            <div className="form_radio">
              <input
                type="radio"
                name="type"
                id="isbn"
                value="isbn"
                onChange={(e) => handleChange(e, "radio")}
              />
              <label htmlFor="isbn">ISBN</label>
            </div>
            <div className="form_radio">
              <input
                type="radio"
                name="type"
                id="author"
                value="author"
                onChange={(e) => handleChange(e, "radio")}
              />
              <label htmlFor="author">Author</label>
            </div>
            <div className="form_radio">
              <input
                type="radio"
                name="type"
                id="character_name"
                value="character_name"
                onChange={(e) => handleChange(e, "radio")}
              />
              <label htmlFor="character_name">Character Name</label>
            </div>
            <div className="form_radio">
              <input
                type="radio"
                name="type"
                id="character_culture"
                value="character_culture"
                onChange={(e) => handleChange(e, "radio")}
              />
              <label htmlFor="character_culture">Character Culture</label>
            </div>
          </div>
        </div>
      </div>
    </SearchWrapper>
  );
};

const SearchWrapper = styled.section`
  .form_group {
    .input_group {
      display: flex;
      min-width: 28rem;
      width: 100%;
      position: relative;
      text-overflow: ellipsis;
      span {
        background: ${colors.colorGray2};
        padding: 1.3rem 1.6rem;
        height: 4.5rem;
        border-top-left-radius: 0.3rem;
        border-bottom-left-radius: 0.3rem;
        border-right: none;
        color: rgba(0, 0, 0, 0.4);
        font-size: 1.4rem;
        display: flex;
        align-items: center;
        justify-content: center;
        cursor: pointer;
      }
      span:last-of-type {
        border-top-left-radius: 0;
        border-bottom-left-radius: 0;
        border-top-right-radius: 0.3rem;
        border-bottom-right-radius: 0.3rem;
      }
      input {
        width: 100%;
        height: 4.5rem;
        border-radius: 0.3rem;
        font-size: 1.4rem;
        padding: 0 1rem;
        color: rgba(0, 0, 0, 0.5);
        border: none;
        background: ${colors.colorGray2};
        border-left: none;
        border-top-left-radius: 0;
        border-bottom-left-radius: 0;
        text-overflow: ellipsis;
        &::placeholder {
          color: ${colors.colorGray1};
        }
      }
    }
    .hide {
      display: none;
    }
    .dropdown {
      position: absolute;
      padding: 1rem;
      background-color: ${colors.colorWhite};
      width: 100%;
      box-shadow: 0px 3px 6px rgba(0, 0, 0, 0.05);
      bottom: -18.5rem;
      p {
        font-weight: 600;
        margin-bottom: 1rem;
      }
    }
    .form_radio {
      display: flex;
      align-items: center;
      margin-bottom: 0.7rem;
      input {
        width: fit-content;
        height: fit-content;
      }
      label {
        margin-left: 0.5rem;
      }
    }
  }
`;

export default SearchComponent;
