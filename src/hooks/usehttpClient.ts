import { AxiosResponse } from "axios";
import { useState } from "react";
import { IResponse } from "../models/response.model";

const useApi = (apiFunc: (args?: any) => AxiosResponse<IResponse[]>) => {
  const [data, setData] = useState<IResponse[]>([]);
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);
  const [hasMore, setHasMore] = useState(true);

  const request = async (...args: any): Promise<AxiosResponse<IResponse[]>> => {
    setLoading(true);
    setError(false);
    const response = await apiFunc(...args);
    setLoading(false);

    setError(response.status !== 200);
    if (response.data.length === 0) setHasMore(false);
    else setData((prev: IResponse[]) => [...prev, ...response.data]);

    return response;
  };

  const updateData = (newData: IResponse[]) => {
    setData(newData);
  };
  const isLoading = (val: boolean) => {
    setLoading(val);
  };

  return { data, error, hasMore, isLoading, loading, request, updateData };
};

export default useApi;
