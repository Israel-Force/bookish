import { useEffect, useState } from "react";

import "./App.scss";
import Navbar from "src/components/Navbar";
import Hero from "src/components/hero";
import Home from "src/pages/Home";
import useApi from "./hooks/usehttpClient";
import bookService from "src/api/services/books";
import Loader from "src/components/shared/Loader";
import NoData from "src/components/shared/NoData";

function App() {
  const {
    data,
    error,
    hasMore,
    loading,
    request: loadBooks,
    updateData,
  } = useApi(bookService.getBooks);
  const [page, setPage] = useState(1);

  useEffect(() => {
    loadBooks({
      pageNumber: page,
    });
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const fetchMoreData = () => {
    setPage((prev) => {
      loadBooks({
        pageNumber: prev + 1,
      });
      return prev + 1;
    });
  };

  const reload = () => {
    loadBooks(1);
  };

  return (
    <>
      <Navbar updateData={updateData} />
      <Hero />
      {!error && data.length > 0 && (
        <div>
          <Home books={data} hasMore={hasMore} fetchMoreData={fetchMoreData} />
        </div>
      )}
      {loading && <Loader />}
      {error && <p>Error!</p>}
      {!loading && !error && data.length < 0 && (
        <div>
          <NoData action={reload} />
        </div>
      )}
    </>
  );
}

export default App;
