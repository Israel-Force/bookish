interface colorModel {
  colorBlack: string;
  colorBlackHex: string;
  colorWhite: string;
  colorBlue: string;
  colorGray: string;
  colorGray1: string;
  colorGray2: string;
  colorGreen: string;
  colorOrange: string;
  colorOrange1: string;
}

const colors: colorModel = {
  colorBlackHex: "0,0,0",
  colorBlack: "#000000",
  colorBlue: "#0038c7",
  colorGray: "#f3f3f3",
  colorGray1: "#a39e9e",
  colorGray2: "#f6f5f3",
  colorGreen: "#0bc255",
  colorOrange: "#ff5c00",
  colorOrange1: "#f75454",
  colorWhite: "#ffffff",
};

export default colors;
