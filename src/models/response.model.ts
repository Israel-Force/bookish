export interface IResponse {
  url: string;
  name: string;
  authors: Array<string>;
  numberOfPages: number;
  publisher: string;
  country: string;
  mediaType: string;
  released: string;
  characters: string[];
  povCharacters: string[];
  isbn: string;

  //   character
  gender?: string;
  culture?: string;
  born?: string;
  died?: string;
  title?: string[];
  aliases?: string[];
  father?: string;
  mother?: string;
  spouse?: string;
  allegiances?: string[];
  books?: string[];
  povBooks?: string[];
  tvSeries?: string[];
  playedBy?: string[];
}
