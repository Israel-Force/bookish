import React from "react";
import { render } from "@testing-library/react";
import App from "./App";

test("App renders Navbar component", () => {
  const { queryByTestId } = render(<App />);
  expect(queryByTestId("navbar")).toBeTruthy();
});

test("App renders Hero component", () => {
  const { queryByTestId } = render(<App />);
  expect(queryByTestId("hero")).toBeTruthy();
});
