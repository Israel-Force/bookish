import axios from "axios";

const httpClient = axios.create({
  baseURL: `https://www.anapioficeandfire.com/api`,
});

export { httpClient };
