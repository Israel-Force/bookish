import "reflect-metadata";
import { Service } from "typedi";
import { AxiosRequestHeaders } from "axios";

import { httpClient as http } from "../client";
@Service()
export default class HttpService {
  getRequest<T>(endpoint: string): Promise<T> | any {
    return http.get(endpoint);
  }

  getRequestWithParams<T>(endpoint: string, params = {}): Promise<T> | any {
    return http.get(endpoint, {
      params,
    });
  }

  makeRequestWithData<T>(
    method: "post" | "put" | "patch" | "get",
    url: string,
    params: any,
    data?: any,
    headers: AxiosRequestHeaders = {
      "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
    }
  ): Promise<T> | any {
    return http.request({
      method,
      url,
      params,
      headers,
      data,
    });
  }
}
