import "reflect-metadata";
import HttpService from "./httpService";

const endpoint = `/books`;

const http = new HttpService();

interface IQueryParams {
  author?: string;
  culture?: string;
  isbn?: string | number;
  name?: string;
  pageNumber: number;
  pageSize?: number;
  publisher?: string;
}

const getBooks = (query: IQueryParams) => {
  const params = {
    page: query.pageNumber ?? 1,
    pageSize: query.pageSize ?? 10,
    author: query.author ?? "",
    culture: query.culture ?? "",
    isbn: query.isbn ?? "",
    name: query.name ?? "",
    publisher: query.publisher ?? "",
  };
  return http.getRequestWithParams(endpoint, params);
};

const books = {
  getBooks,
};

export default books;
