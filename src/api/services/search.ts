import "reflect-metadata";
import Container, { Service } from "typedi";
import { AxiosResponse } from "axios";
import { forkJoin, from, map, mergeMap, Observable } from "rxjs";

import HttpService from "./httpService";
import { IResponse } from "src/models/response.model";

@Service()
class SearchService {
  private http = Container.get(HttpService);
  private bookEndpoint = `/books`;
  private charactersEndpoint = `/characters`;

  getBooksByQuery(
    query: string,
    queryParams: string | number
  ): Observable<IResponse[]> {
    const params = {
      [query]: queryParams,
    };
    const request: Promise<AxiosResponse<IResponse[]>> =
      this.http.getRequestWithParams(this.bookEndpoint, params);
    return from(request).pipe(
      map((res) => {
        console.log(res.data);
        return res.data;
      })
    );
  }

  getBooksByCharacter(query: string, queryParams: string | number) {
    const params = {
      [query]: queryParams,
    };
    const characters = from(
      this.http.getRequestWithParams(this.charactersEndpoint, params)
    );
    return characters.pipe(
      mergeMap((result: any) => {
        let response = result.data.map((res: IResponse) => res.books);
        let bookUrls: string[] = [];
        for (const val of response) {
          bookUrls = bookUrls.concat(val);
        }
        const endpoints = bookUrls.map((bookUrl) =>
          from(this.http.getRequest(bookUrl))
        );
        return forkJoin(endpoints);
      }),
      map((results: any) => {
        results = results.map((result: any) => result.data);
        return results;
      })
    );
  }
}

export default SearchService;
