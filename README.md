## About Bookish

Bookish is a book inventory websitet\
that allows for users to browse through various book collections.

## Technologies

Html\
SCSS\
Typedcript\
React Js\

## Getting Started

Clone the repository by copying the link or downloading the zip file

## Requirement

You must have node js installed on your computer to run the application\
Downlaod node js for windows (x64) here:\
https://nodejs.org/dist/v14.15.4/node-v14.15.4-x64.msi \
for Mac users:\
https://nodejs.org/dist/v14.15.4/node-v14.15.4.pkg

## Environmental variable

### PLEASE TREAT AS IMPORTANT:

- kindly make sure you have a .env file in\
  your root folder that has the content below:\
  SASS_PATH=src

NB: If you do not have a .env file please create one\
 and copy paste the content above else scss won't compile properly.

## Available Scripts

In the project directory, run:

### `npm install` to install all dependencies

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## After starting the server locally

Kindly hover on each list of books to see their complete details

### `npm test`

Launches the test runner in the interactive watch mode.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!
